package com.clearlane.blueopsjobs.model;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Configurable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Davit Hakobyan
 * @version 1.0
 * @since 3/3/20 - 5:04 PM
 */

@Entity
@Configurable
@Table(name = "applicant")
public class Applicant implements Serializable {

    /**
     * <class-descriptor class="com.blueyield.model.ApplicantDTO" table="applicant" >
     * <field-descriptor name="id" column="id" jdbc-type="INTEGER" primarykey="true" autoincrement="true" />
     * <field-descriptor name="firstName" column="firstName" jdbc-type="VARCHAR" />
     * <field-descriptor name="middleName" column="middleName" jdbc-type="VARCHAR" />
     * <field-descriptor name="lastName" column="lastName" jdbc-type="VARCHAR" />
     * <field-descriptor name="nameSuffix" column="nameSuffix" jdbc-type="VARCHAR" />
     * <field-descriptor name="ssn" column="ssn" jdbc-type="VARCHAR" />
     * <field-descriptor name="dob" column="dob" jdbc-type="DATE" />
     * <field-descriptor name="addressStreet" column="addressStreet" jdbc-type="VARCHAR" />
     * <field-descriptor name="addressCity" column="addressCity" jdbc-type="VARCHAR" />
     * <field-descriptor name="addressState" column="addressState" jdbc-type="VARCHAR" />
     * <field-descriptor name="addressTimeZone" column="addressTimeZone" jdbc-type="VARCHAR" />
     * <field-descriptor name="addressZip" column="addressZip" jdbc-type="VARCHAR" />
     * <field-descriptor name="yearsAtAddress" column="yearsAtAddress" jdbc-type="INTEGER" />
     * <field-descriptor name="monthsAtAddress" column="monthsAtAddress" jdbc-type="INTEGER" />
     * ....
     * <field-descriptor name="driverLicense" column="driverLicense" jdbc-type="VARCHAR" />
     * <field-descriptor name="driverLicenseState" column="driverLicenseState" jdbc-type="VARCHAR" />
     * <field-descriptor name="driverLicenseIssueDate" column="driverLicenseIssueDate" jdbc-type="DATE" />
     * <field-descriptor name="driverLicenseExpireDate" column="driverLicenseExpireDate" jdbc-type="DATE" />
     * </class-descriptor>
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    private String firstName;
    private String middleName;
    private String lastName;
    private String nameSuffix;
    private Date dob;

    //    private String addressStreet;
    private String addressStreetNumber;
    private String addressCity;
    private String addressState;
    private String addressTimeZone;
    private String addressZip;

    private String driverLicense;
    private String driverLicenseState;
    private Date driverLicenseIssueDate;
    private Date driverLicenseExpireDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNameSuffix() {
        return nameSuffix;
    }

    public void setNameSuffix(String nameSuffix) {
        this.nameSuffix = nameSuffix;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

//    public String getAddressStreet() {
//        return addressStreet;
//    }
//
//    public void setAddressStreet(String addressStreet) {
//        this.addressStreet = addressStreet;
//    }

    public String getAddressStreetNumber() {
        return addressStreetNumber;
    }

    public Applicant setAddressStreetNumber(String addressStreetNumber) {
        this.addressStreetNumber = addressStreetNumber;
        return this;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public String getAddressState() {
        return addressState;
    }

    public void setAddressState(String addressState) {
        this.addressState = addressState;
    }

    public String getAddressTimeZone() {
        return addressTimeZone;
    }

    public void setAddressTimeZone(String addressTimeZone) {
        this.addressTimeZone = addressTimeZone;
    }

    public String getAddressZip() {
        return addressZip;
    }

    public void setAddressZip(String addressZip) {
        this.addressZip = addressZip;
    }

    public String getDriverLicense() {
        return driverLicense;
    }

    public void setDriverLicense(String driverLicense) {
        this.driverLicense = driverLicense;
    }

    public String getDriverLicenseState() {
        return driverLicenseState;
    }

    public void setDriverLicenseState(String driverLicenseState) {
        this.driverLicenseState = driverLicenseState;
    }

    public Date getDriverLicenseIssueDate() {
        return driverLicenseIssueDate;
    }

    public void setDriverLicenseIssueDate(Date driverLicenseIssueDate) {
        this.driverLicenseIssueDate = driverLicenseIssueDate;
    }

    public Date getDriverLicenseExpireDate() {
        return driverLicenseExpireDate;
    }

    public void setDriverLicenseExpireDate(Date driverLicenseExpireDate) {
        this.driverLicenseExpireDate = driverLicenseExpireDate;
    }

    public String composeFullName() {
        StringBuilder sb = new StringBuilder();
        if (StringUtils.isNotEmpty(firstName))
            sb.append(firstName.trim()).append(" ");
        if (StringUtils.isNotEmpty(middleName))
            sb.append(middleName.trim()).append(" ");
        if (StringUtils.isNotEmpty(lastName))
            sb.append(lastName.trim()).append(" ");
        if (StringUtils.isNotEmpty(nameSuffix))
            sb.append(nameSuffix.trim());
        return sb.toString().trim();
    }

    /**
     * trim all the leading and trailing spaces
     */
    public Applicant trim() {
        if (firstName!=null) firstName = firstName.trim();
        if (lastName!=null) lastName = lastName.trim();
        if (driverLicense!=null) driverLicense = driverLicense.trim();
        if (driverLicenseState!=null) driverLicenseState = driverLicenseState.trim();
        if (addressStreetNumber!=null) addressStreetNumber = addressStreetNumber.trim();
        if (addressCity!=null) addressCity = addressCity.trim();
        if (addressZip!=null) addressZip = addressZip.trim();
        return this;
    }
}