package com.clearlane.blueopsjobs.model;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Davit Hakobyan
 * @version 1.0
 * @since 3/4/20 - 5:38 PM
 */
@Entity
@Configurable
@Table(name = "attachment")
public class Attachment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    private Integer attachmentType;
    private String directoryName;
    private String s3fileName;

    @Column(name = "autoValidationComments", columnDefinition="TEXT")
    private String autoValidationComments;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date updated;

    public Attachment() {
    }

    public Integer getId() {
        return id;
    }

    public Integer getAttachmentType() {
        return attachmentType;
    }

    public String getDirectoryName() {
        return directoryName;
    }

    public String getS3fileName() {
        return s3fileName;
    }

    public Boolean isDeleted() {
        return isDeleted;
    }

    public String getAutoValidationComments() {
        return autoValidationComments;
    }

    public void setAutoValidationComments(String autoValidationComments) {
        this.autoValidationComments = autoValidationComments;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
}
