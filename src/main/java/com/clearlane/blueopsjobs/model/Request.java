package com.clearlane.blueopsjobs.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.beans.factory.annotation.Configurable;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Davit Hakobyan
 * @version 1.0
 * @since 3/3/20 - 5:01 PM
 */

@Entity
@Configurable
@Table(name = "request")
public class Request implements Serializable {

    /**
     * <class-descriptor class="com.blueyield.model.RequestDTO" table="request">
     *     <field-descriptor name="id" column="id" jdbc-type="INTEGER" primarykey="true" autoincrement="true"/>
     *     <field-descriptor name="primaryApplicantId" column="preparedById" jdbc-type="INTEGER" />
     *     <field-descriptor name="coApplicantId" column="coApplicantId" jdbc-type="INTEGER" />
     *     <field-descriptor name="comments" column="comments" jdbc-type="LONGVARCHAR" />
     *     ....
     * </class-descriptor>
     */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    private Integer primaryApplicantId;
    private Integer coApplicantId;

    @Column(name = "comments", columnDefinition="MEDIUMTEXT")
    private String comments;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPrimaryApplicantId() {
        return primaryApplicantId;
    }

    public void setPrimaryApplicantId(Integer primaryApplicantId) {
        this.primaryApplicantId = primaryApplicantId;
    }

    public Integer getCoApplicantId() {
        return coApplicantId;
    }

    public void setCoApplicantId(Integer coApplicantId) {
        this.coApplicantId = coApplicantId;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
