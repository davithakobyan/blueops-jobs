package com.clearlane.blueopsjobs.model;

/**
 * @author Davit Hakobyan
 * @version 1.0
 * @since 2/8/21 - 2:48 PM
 */
public interface Constants {
    String SPRING_PROFILES_ACTIVE = "spring.profiles.active";
    String LOCAL_ENVIRONMENT = "local";
    String TIME_ZONE = "America/Los_Angeles";
}
