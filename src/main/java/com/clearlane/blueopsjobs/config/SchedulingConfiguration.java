package com.clearlane.blueopsjobs.config;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author Davit Hakobyan
 * @version 1.0
 * @since 2/8/21 - 11:38 AM
 */
@EnableScheduling
@Configuration
@ConditionalOnProperty(name = "scheduling.enabled", matchIfMissing = true)
class SchedulingConfiguration {
    private static final Logger logger = LoggerFactory.getLogger(SchedulingConfiguration.class);
}