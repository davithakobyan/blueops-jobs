package com.clearlane.blueopsjobs.config;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.clearlane.blueopsjobs.properties.AmazonProperties;
import com.clearlane.blueopsjobs.util.Environment;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Davit Hakobyan
 * @version 1.0
 * @since 2/8/21 - 2:45 PM
 */
@Configuration
public class AmazonClientConfig {

    private static final Logger logger = LoggerFactory.getLogger(AmazonClientConfig.class);

    private final AmazonProperties amazonProperties;

    @Autowired
    public AmazonClientConfig(AmazonProperties amazonProperties) {
        this.amazonProperties = amazonProperties;
    }

    @Bean
    public AmazonS3 amazonS3() {
        logger.info("amazonS3()");
        ClientConfiguration clientConfig = new ClientConfiguration();
        clientConfig.setProtocol(Protocol.HTTPS);

        if (Environment.isLocalEnvironment()) {
            String proxyHost = System.getProperty("http.proxyHost");
            int proxyPort = Integer.getInteger("http.proxyPort");
            clientConfig.setProxyHost(proxyHost);
            clientConfig.setProxyPort(proxyPort);

            String proxyUser = System.getProperty("http.proxyUser");
            String proxyPassword = System.getProperty("http.proxyPassword");
            clientConfig.setProxyUsername(proxyUser);
            clientConfig.setProxyPassword(proxyPassword);
        }

        AmazonS3ClientBuilder s3Builder = AmazonS3ClientBuilder.standard()
                .withRegion(amazonProperties.getRegion());
        s3Builder.withClientConfiguration(clientConfig);

        return s3Builder.build();
    }
}
