package com.clearlane.blueopsjobs;

import com.clearlane.blueopsjobs.jobs.AllyBookingReportJob2;
import com.clearlane.blueopsjobs.model.Request;
import com.clearlane.blueopsjobs.repository.RequestRepository;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @author Davit Hakobyan
 * @version 1.0
 * @since 2/8/21 - 4:02 PM
 */
@Component
public class BlueopsJobsRunner implements CommandLineRunner {
    private static final Logger logger = LoggerFactory.getLogger(BlueopsJobsRunner.class);

    @Autowired
    Scheduler scheduler;

    @Override
    public void run(String... args) throws Exception {
        AllyBookingReportJob2.schedule(scheduler);
        scheduler.start();
    }
}
