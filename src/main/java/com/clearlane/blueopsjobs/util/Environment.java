package com.clearlane.blueopsjobs.util;

import com.clearlane.blueopsjobs.model.Constants;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * @author Davit Hakobyan
 * @version 1.0
 * @since 2/17/21 - 1:36 PM
 */
public class Environment {

    public static boolean isLocalEnvironment() {
        String profile = System.getProperty(Constants.SPRING_PROFILES_ACTIVE);
        String environment = (profile != null) ? profile : Constants.LOCAL_ENVIRONMENT;
        return Constants.LOCAL_ENVIRONMENT.equalsIgnoreCase(environment);
    }
}
