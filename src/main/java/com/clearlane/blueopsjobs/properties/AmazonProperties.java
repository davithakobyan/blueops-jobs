package com.clearlane.blueopsjobs.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

/**
 * @author Davit Hakobyan
 * @version 1.0
 * @since 2/17/21 - 1:26 PM
 */
@ConfigurationProperties(prefix = "amazon", ignoreUnknownFields = false)
public class AmazonProperties {

    private String region;
    private String bucketName;

    @ConstructorBinding
    public AmazonProperties(String region, String bucketName) {
        this.region = region;
        this.bucketName = bucketName;
    }

    public String getRegion() {
        return region;
    }

    public String getBucketName() {
        return bucketName;
    }
}
