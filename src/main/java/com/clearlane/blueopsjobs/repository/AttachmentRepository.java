package com.clearlane.blueopsjobs.repository;

import com.clearlane.blueopsjobs.model.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @author Davit Hakobyan
 * @version 1.0
 * @since 3/4/20 - 5:44 PM
 */
@Repository
public interface AttachmentRepository extends JpaRepository<Attachment, Integer> {

    @Modifying
    @Transactional
    @Query(value = "UPDATE attachment SET autoValidationComments = :errorMessages, updated = NOW() WHERE id = :id", nativeQuery = true)
    public Integer updateAutoValidationComments(@Param("id") Integer id, @Param("errorMessages") String errorMessages);

    @Modifying
    @Transactional
    @Query(value = "UPDATE attachment SET autoValidationComments = :errorMessages, updated = :updated WHERE id = :id", nativeQuery = true)
    public Integer updateAutoValidationComments(@Param("id") Integer id, @Param("errorMessages") String errorMessages, @Param("updated") Date updated);

    @Query(value = "SELECT * FROM attachment WHERE is_deleted = false and id = :attachmentId", nativeQuery = true)
    Attachment getAttachmentNotDeleted(@Param("attachmentId") Integer attachmentId);

}