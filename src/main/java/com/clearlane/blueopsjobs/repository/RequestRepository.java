package com.clearlane.blueopsjobs.repository;

import com.clearlane.blueopsjobs.model.Request;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author Davit Hakobyan
 * @version 1.0
 * @since 2/8/21 - 11:41 AM
 */
@Repository
public interface RequestRepository extends JpaRepository<Request, Integer> {


    @Override
    Optional<Request> findById(Integer requestId);
}
