package com.clearlane.blueopsjobs.repository;

import com.clearlane.blueopsjobs.model.Applicant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Davit Hakobyan
 * @version 1.0
 * @since 3/3/20 - 5:31 PM
 */
@Repository
public interface ApplicantRepository extends JpaRepository<Applicant, Integer> {

}
