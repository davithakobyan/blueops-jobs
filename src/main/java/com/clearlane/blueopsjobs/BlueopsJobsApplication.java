package com.clearlane.blueopsjobs;

import com.clearlane.blueopsjobs.properties.AmazonProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({AmazonProperties.class})
public class BlueopsJobsApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlueopsJobsApplication.class, args);
	}

}

