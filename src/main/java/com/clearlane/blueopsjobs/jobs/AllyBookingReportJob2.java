package com.clearlane.blueopsjobs.jobs;

import com.clearlane.blueopsjobs.model.Constants;
import org.quartz.*;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.TimeZone;

import static org.quartz.CronScheduleBuilder.cronSchedule;

/**
 * @author Davit Hakobyan
 * @version 1.0
 * @since 2/8/21 - 4:53 PM
 */
@Component
public class AllyBookingReportJob2 implements Job {
    private static final Logger logger = LoggerFactory.getLogger(AllyBookingReportJob2.class);
    
    private static String JOB_FREQUENCY = "0/5 * * ? * *";

    public static void schedule(Scheduler scheduler) {
        logger.info("In Booking Report File Generator Job Schedule");
        try {
            JobDetail jd = JobBuilder.newJob(AllyBookingReportJob2.class)
                    .withIdentity("AllyBookingReportJob", Scheduler.DEFAULT_GROUP)
                    .storeDurably(true).build();

            CronTrigger trig = TriggerBuilder.newTrigger()
                    .withIdentity("AllyBookingReportJobTrigger", Scheduler.DEFAULT_GROUP)
                    .withSchedule(cronSchedule(JOB_FREQUENCY)
                    .inTimeZone(TimeZone.getTimeZone(Constants.TIME_ZONE)))
                    .build();

            scheduler.scheduleJob(jd, trig);

            logger.warn("AllyBookingReportJob scheduled with frequency : " + JOB_FREQUENCY);
        }
        catch (Exception e) {
            logger.error("ERROR!! ERROR!! Could not start AllyBookingReportJob " + e.getMessage(), e);
        }
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        logger.info("execute() Now is: {}", new Date());
    }
}
