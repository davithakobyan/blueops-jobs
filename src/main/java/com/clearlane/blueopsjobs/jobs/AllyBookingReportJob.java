package com.clearlane.blueopsjobs.jobs;

import com.clearlane.blueopsjobs.model.Request;
import com.clearlane.blueopsjobs.repository.RequestRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * @author Davit Hakobyan
 * @version 1.0
 * @since 2/8/21 - 12:19 PM
 */
@Component
public class AllyBookingReportJob {

    private static final Logger logger = LoggerFactory.getLogger(AllyBookingReportJob.class);

    @Autowired
    private RequestRepository requestRepository;

    @Scheduled(cron = "${blueops.AllyBookingReportJob.frequency}")
    void shoeJob() {
        logger.info("shoeJob() Now is: {}", new Date());
//        List<Request> requestList = requestRepository.findAll();
//        logger.info("requestRepository.findAll().size(): {}", requestList.size());
    }

}
