package com.clearlane.blueopsjobs.service;

import com.clearlane.blueopsjobs.model.Request;
import com.clearlane.blueopsjobs.repository.RequestRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Davit Hakobyan
 * @version 1.0
 * @since 2/17/21 - 11:19 AM
 */
@Service
public class RequestService {

    private static final Logger logger = LoggerFactory.getLogger(RequestService.class);

    private final RequestRepository requestRepository;

    @Autowired
    public RequestService(RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }

    public Request createRequest(Request newRequest) {
        logger.info("createRequest()");
        return requestRepository.save(newRequest);
    }

    public Request findRequestById(Integer requestId) {
        logger.info("findRequestById()");
        return requestRepository.findById(requestId).orElse(null);
    }

    public void deleteById(Integer requestId) {
        logger.info("deleteById()");
        requestRepository.deleteById(requestId);
    }

}
