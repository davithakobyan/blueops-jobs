package com.clearlane.blueopsjobs.service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Davit Hakobyan
 * @version 1.0
 * @since 2/8/21 - 2:50 PM
 */
@Service
public class AmazonS3Client {

    private static final Logger logger = LoggerFactory.getLogger(AmazonS3Client.class);

    private final AmazonS3 s3client;

    @Autowired
    public AmazonS3Client(AmazonS3 s3client) {
        this.s3client = s3client;
    }

    /**
     * Check if the stipulation file is available in S3 bucket
     *
     * @param bucket   String
     * @param fileName String
     * @return boolean
     */
    boolean fileExists(String bucket, String fileName) {
        boolean exists = false;
        try {
            exists = s3client.doesObjectExist(bucket, fileName);
            logger.info("Driver's License image exists in : " + exists);
        } catch (AmazonServiceException ase) {
            logger.error("Amazon service exception has occurred. ", ase);
        }
        return exists;
    }

}