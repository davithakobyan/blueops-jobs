package com.clearlane.blueopsjobs;

import static com.clearlane.blueopsjobs.model.Constants.LOCAL_ENVIRONMENT;

/**
 * @author Davit Hakobyan
 * @version 1.0
 * @since 2/18/21 - 4:58 PM
 */

public class PrepEnvironment {

    public PrepEnvironment() {
        loadEnvironment();
    }

    private void loadEnvironment() {
        System.setProperty("env", LOCAL_ENVIRONMENT);
        System.setProperty("spring.profiles.active", LOCAL_ENVIRONMENT);
        System.setProperty("http.proxyHost", "10.43.196.141");
        System.setProperty("http.proxyPort", "80");
        System.setProperty("http.proxyUser", "dhakobyan");
        System.setProperty("http.proxyPassword", "secr3t");
    }
}
