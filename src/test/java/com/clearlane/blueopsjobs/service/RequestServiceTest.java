package com.clearlane.blueopsjobs.service;

import com.clearlane.blueopsjobs.BlueopsJobsApplication;
import com.clearlane.blueopsjobs.PrepEnvironment;
import com.clearlane.blueopsjobs.model.Request;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import java.net.InetAddress;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Davit Hakobyan
 * @version 1.0
 * @since 2/17/21 - 11:31 AM
 */
@SpringBootTest(classes = BlueopsJobsApplication.class)
@TestPropertySource(locations = "classpath:application-test.yml")
class RequestServiceTest extends PrepEnvironment {

    @Autowired
    RequestService requestService;

    @BeforeEach
    void setUp() {
        assertNotNull(requestService);
    }

    @Test
    void createFindDropRequest() {
        Request request = new Request();
        request.setPrimaryApplicantId(111);
        request.setCoApplicantId(222);
        request.setComments("Unit test");
        requestService.createRequest(request);
        assertNotNull(request.getId());

        Request requestById = requestService.findRequestById(request.getId());
        assertEquals(requestById.getId(), request.getId());

        requestService.deleteById(requestById.getId());
        requestById = requestService.findRequestById(request.getId());
        assertNull(requestById);
    }

}