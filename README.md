# Blueops Jobs #

----

Blueops-jobs is the decoupled from the original blueops project. The purpose is to reengineer all the important job components, migrated them to modern frameworks, improve reliability and maintainability.
Please see the [confluence page](https://byjira.atlassian.net/wiki/spaces/BY/pages/2364506113/Blueops+Job+Analysis+WIP)


### Prerequirements ###

* apache maven 3.x
* jdk8.x

### How do I get set up? ###

* clone the repository from the [bitbucket](https://bitbucket.org/blueyieldtech/) to your local system.
    
```
git clone https://bitbucket.org/blueyieldtech/blueops-jobs/src/dev/
```
    
* cd to blueops-jobs folder

### How to build the app using maven ###
Use the maven commands to build the stipulations service app
``` 
mvn clean package
```
        
### How to run the blueops-jobs with as spring boot ###
Use the maven commands to start spring boot applications from the terminal    
```
mvn spring-boot:run -Dspring.profiles.active=<ENVIRONMENT-NAME>
```

### How to run the blueops-jobs as standalone java application ###
Use the `java` commands with `jar` option to start applications from the terminal 

```
java -jar blueops-jobs.jar -Dspring.profiles.active=<ENVIRONMENT-NAME>
```

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* For future questions please contact [Davit Hakobyan](davit.hakobyan@clearlane.com)
* Other community or team contact


### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.2/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.2/maven-plugin/reference/html/#build-image)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.4.2/reference/htmlsingle/#boot-features-jpa-and-spring-data)
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/2.4.2/reference/htmlsingle/#using-boot-devtools)
* [Spring Boot Actuator](https://docs.spring.io/spring-boot/docs/2.4.2/reference/htmlsingle/#production-ready)

### Guides
The following guides illustrate how to use some features concretely:

* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)
* [Accessing data with MySQL](https://spring.io/guides/gs/accessing-data-mysql/)
